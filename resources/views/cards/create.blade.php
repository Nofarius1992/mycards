@extends('layout')

@include('errors');

@section('content')
    <div class="container">

        <h3>Create card</h3>

        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['route' => ['cards.store']]) !!}

                <div class="form-group">
                    <input name="title" type="text" class="form-control" value="{{ old('title') }}">
                    <br>
                    <textarea name="description"
                              id=""
                              cols="30"
                              rows="10"
                              class="form-control">{{ old('description') }}</textarea>
                    <br>
                    <button class="btn btn-success">Submit</button>
                    <a class="btn btn-success" href="{{ route('cards.index') }}">Назад</a>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

