@extends('layout')

@section('content')
    <div class="container">

        <h3>My cards</h3>
        <a href="{{ route('cards.create') }}" class="btn btn-success">Create</a>
        <div class="row">
            <div class="col-md-12 col-md-offset-1">
                <table class="table">
                    <thead>
                    <tr>
                        <td>Id</td>
                        <td>Title</td>
                        <td>Actions</td>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($cards as $card)
                    <tr >
                        <td>{{ $card->id }}</td>
                        <td>{{ $card->title }}</td>
                        <td style="display: flex">
                            <a href="{{ route('cards.show', $card) }}" class="btn btn-success">Open</a>
                            <a href="{{ route('cards.edit', $card) }}" class="btn btn-success">Edit</a>
                            {!! Form::open(['method' => 'DELETE',
                              'route' => ['cards.destroy', $card->id]]) !!}
                            <button onclick="return confirm('Are you sure?')" class="btn btn-danger">
                                Delete
                            </button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
