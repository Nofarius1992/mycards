@extends('layout')

@section('content')
    <div class="container">
        <div class="col-md-12 text-center">
            <h3>{{ $card->title }}</h3>
            <p>{{ $card->description }}</p>
            <p>{{ $card->updated_at }}</p>
            <a href="{{ route('cards.index') }}" class="btn btn-success">Назад</a>
        </div>
    </div>
@endsection
