@extends('layout')

@include('errors')
@section('content')
    <div class="container">
        <h3>Edit card # - {{ $card->id }}</h3>
        <div class="row">
            <div class="col-md-9">
                {!! Form::open(['route' => ['cards.update', $card->id], 'method' => 'PUT']) !!}

                <div class="form-group">
                    <input name="title" type="text" class="form-control" value="{{ $card->title }}">
                    <br>
                    <textarea name="description"
                              id=""
                              cols="30"
                              rows="10"
                              class="form-control">{{ $card->description }}</textarea>
                    <br>
                    <button class="btn btn-success">Update</button>
                    <a class="btn btn-success" href="{{ route('cards.index') }}">Назад</a>
                </div>

                {!! Form::close() !!}
            </div>
            <div class="col-md-3.1">
                <div class="text-center">
                    <h5>Update</h5>
                    <input class="form-control text-center" type="text" value="{{ $card->updated_at }}" readonly="readonly">
                </div>
            </div>
        </div>
    </div>
@endsection
