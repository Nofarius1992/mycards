<?php

Route::get('/', function() {return view('cards.home');})->name('home.page');

Route::get('cards', 'CardsController@index')->name('cards.index');

Route::get('cards/create', 'CardsController@create')->name('cards.create');

Route::post('cards/store', 'CardsController@store')->name('cards.store');

Route::get('cards/{id}/show', 'CardsController@show')->name('cards.show');

Route::get('cards/{id}/edit', 'CardsController@edit')->name('cards.edit');

Route::put('cards/{id}/update', 'CardsController@update')->name('cards.update');

Route::delete('cards/{id}/destroy', 'CardsController@destroy')->name('cards.destroy');
